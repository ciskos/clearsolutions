
Java practical test assignment

The task has two parts:
1. Using the resources listed below learn what is RESTful API and what are the best practices to implement it 
2. According to the requirements implement the RESTful API based on the web Spring Boot application: controller, responsible for the resource named Users. 

Resources:
1. [RESTful API Design. Best Practices in a Nutshell.](https://phauer.com/2015/restful-api-design-best-practices/)
2. [Error Handling for REST with Spring | Baeldung](https://www.baeldung.com/exception-handling-for-rest-with-spring)
3. [Testing in Spring Boot | Baeldung](https://www.baeldung.com/spring-boot-testing#unit-testing-with-webmvctest)
4. [Testing | Spring](https://docs.spring.io/spring-framework/reference/testing/spring-mvc-test-framework/server.html)

Requirements:
1. It has the following fields:
1.1. Email (required). Add validation against email pattern
1.2. First name (required)
1.3. Last name (required)
1.4. Birth date (required). Value must be earlier than current date
1.5. Address (optional)
1.6. Phone number (optional)
2. It has the following functionality:
2.1. Create user. It allows to register users who are more than [18] years old. The value [18] should be taken from properties file.
2.2. Update one/some user fields
2.3. Update all user fields
2.4. Delete user
2.5. Search for users by birth date range. Add the validation which checks that “From” is less than “To”.  Should return a list of objects
3. Code is covered by unit tests using Spring 
4. Code has error handling for REST
5. API responses are in JSON format
6. Use of database is not necessary
7. Latest version of Spring Boot. Java version of your choice
8. You can use Spring Initializer utility to create the project: [Spring Initializr](https://start.spring.io/)

Please note: 
we assess only those assignments where all requirements are implemented

===

USERS

GET - getAllUsers
curl -v -H 'Accept: application/json' http://localhost:8080/users

[{"id":1,"email":"email1@mail.com","firstName":"firstname1","lastName":"lastname1","birthday":[1980,1,1],"address":"address1","phoneNumber":"+38(063)123-12-34"},{"id":2,"email":"email2@mail.com","firstName":"firstname2","lastName":"lastname2","birthday":[1981,2,2],"address":"address2","phoneNumber":"+38(063)123 12 34"},{"id":3,"email":"email3@mail.com","firstName":"firstname3","lastName":"lastname3","birthday":[1982,3,3],"address":"address3","phoneNumber":"+38(063) 123 12 34"},{"id":4,"email":"email4@mail.com","firstName":"firstname4","lastName":"lastname4","birthday":[1983,4,4],"address":"address4","phoneNumber":"+38(063)-123-12-34"}]

---

GET - getUsersById
curl -v -H 'Accept: application/json' http://localhost:8080/users/1

{"id":1,"email":"email1@mail.com","firstName":"firstname1","lastName":"lastname1","birthday":[1980,1,1],"address":"address1","phoneNumber":"+38(063)123-12-34"}

---

POST - getAllusersByDateRange

curl -X POST -vd '{"range":{"from":"1900-01-01","to":"2000-01-01"}}' -H 'Content-Type: application/json' http://localhost:8080/users/daterange

[{"id":1,"email":"email1@mail.com","firstName":"firstname1","lastName":"lastname1","birthday":[1980,1,1],"address":"address1","phoneNumber":"+38(063)123-12-34"},{"id":2,"email":"email2@mail.com","firstName":"firstname2","lastName":"lastname2","birthday":[1981,2,2],"address":"address2","phoneNumber":"+38(063)123 12 34"},{"id":3,"email":"email3@mail.com","firstName":"firstname3","lastName":"lastname3","birthday":[1982,3,3],"address":"address3","phoneNumber":"+38(063) 123 12 34"},{"id":4,"email":"email4@mail.com","firstName":"firstname4","lastName":"lastname4","birthday":[1983,4,4],"address":"address4","phoneNumber":"+38(063)-123-12-34"}]

---

POST - getAllusersByDateRange - smaller range

curl -X POST -vd '{"range":{"from":"1981-01-01","to":"2000-01-01"}}' -H 'Content-Type: application/json' http://localhost:8080/users/daterange

[{"id":2,"email":"email2@mail.com","firstName":"firstname2","lastName":"lastname2","birthday":[1981,2,2],"address":"address2","phoneNumber":"+38(063)123 12 34"},{"id":3,"email":"email3@mail.com","firstName":"firstname3","lastName":"lastname3","birthday":[1982,3,3],"address":"address3","phoneNumber":"+38(063) 123 12 34"},{"id":4,"email":"email4@mail.com","firstName":"firstname4","lastName":"lastname4","birthday":[1983,4,4],"address":"address4","phoneNumber":"+38(063)-123-12-34"}]

---

POST - getAllusersByDateRange wrong dates

curl -X POST -vd '{"range":{"from":"2000-01-01","to":"1900-01-01"}}' -H 'Content-Type: application/json' http://localhost:8080/users/daterange

{"range":"Start date must be before end date"}

---

POST - addUsers
curl -X POST -vd '{"email":"emailNEW@mail.com","firstName":"firstname5","lastName":"lastname5","birthday":[1980,5,5],"address":"address5","phoneNumber":"+38(098)321-21-43"}' -H 'Content-Type: application/json' http://localhost:8080/users

{"id":5,"email":"emailNEW@mail.com","firstName":"firstname5","lastName":"lastname5","birthday":[1980,5,5],"address":"address5","phoneNumber":"+38(098)321-21-43"}

---

POST - addUsers - wrong email, birthday, phoneNumber
curl -X POST -vd '{"email":"emailmail.com","firstName":"firstname5","lastName":"lastname5","birthday":[2080,5,5],"address":"address5","phoneNumber":"+38()321-21-43"}' -H 'Content-Type: application/json' http://localhost:8080/users

{"birthday":"Date must be less than current, User must be older than 18","phoneNumber":"invalid phone format","email":"invalid email format"}

---

UPDATE PUT - putUsers
curl -X PUT -vd '{"id":4,"email":"emailNEW@mail.com","firstName":"firstnameNEW","lastName":"lastnameNEW","birthday":[2000,5,5],"address":"addressNEW","phoneNumber":"+38(098)1234567"}' -H 'Content-Type: application/json' http://localhost:8080/users

{"id":4,"email":"emailNEW@mail.com","firstName":"firstnameNEW","lastName":"lastnameNEW","birthday":[2000,5,5],"address":"addressNEW","phoneNumber":"+38(098)1234567"}

---

UPDATE PUT - putUsers - wrong email, birthday, phoneNumber
curl -X PUT -vd '{"id":4,"email":"emailNEWmail.com","firstName":"firstnameNEW","lastName":"lastnameNEW","birthday":[2100,5,5],"address":"addressNEW","phoneNumber":"+38()1234567"}' -H 'Content-Type: application/json' http://localhost:8080/users

{"birthday":"Date must be less than current, User must be older than 18","phoneNumber":"invalid phone format","email":"invalid email format"}

---

UPDATE PATCH - patchUsers
curl -X PATCH -vd '{"email":"emailPATCHED@mail.com","birthday":[2000,6,6],"phoneNumber":"+38(063)7654321"}' -H 'Content-Type: application/json' http://localhost:8080/users/4

{"id":4,"email":"emailPATCHED@mail.com","firstName":"firstnameNEW","lastName":"lastnameNEW","birthday":[2000,6,6],"address":"addressNEW","phoneNumber":"+38(063)7654321"}

---

UPDATE PATCH - patchUsers - wrong email, birthday, phoneNumber
curl -X PATCH -vd '{"email":"emailPATCHEDmail.com","birthday":[2100,6,6],"phoneNumber":"+38()7654321"}' -H 'Content-Type: application/json' http://localhost:8080/users/4

{"id":4,"email":"emailPATCHEDmail.com","firstName":"firstnameNEW","lastName":"lastnameNEW","birthday":[2100,6,6],"address":"addressNEW","phoneNumber":"+38()7654321"}

---

DELETE - deleteUsers
curl -X DELETE -v http://localhost:8080/users/4

---

ERRORS

curl -X PATCH -vd '{}' -H 'Content-Type: application/json' http://localhost:8080/users/3

{"statusCode":400,"timestamp":[2023,9,28],"message":"could not execute statement [Значение NULL не разрешено для поля \"EMAIL\"\nNULL not allowed for column \"EMAIL\"; SQL statement:\ninsert into users (address,birthday,email,firstname,lastname,phonenumber,id) values (?,?,?,?,?,?,default) [23502-214]] [insert into users (address,birthday,email,firstname,lastname,phonenumber,id) values (?,?,?,?,?,?,default)]; SQL [insert into users (address,birthday,email,firstname,lastname,phonenumber,id) values (?,?,?,?,?,?,default)]; constraint [null]","description":"uri=/users/4"}
