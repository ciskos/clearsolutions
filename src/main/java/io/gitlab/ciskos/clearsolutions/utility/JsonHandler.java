package io.gitlab.ciskos.clearsolutions.utility;

import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class JsonHandler {

	public static String bindingResultToJson(BindingResult bindingResult) throws JsonProcessingException {
		Map<String, String> collect = bindingResult.getFieldErrors().stream()
				.collect(Collectors.toMap(FieldError::getField
						, FieldError::getDefaultMessage
						, (oldValue, newValue) -> oldValue + ", " + newValue));
		
		return objectToJson(collect);
	}

	public static String objectToJson(Object input) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		
		mapper.registerModule(new JavaTimeModule());
		
		return mapper.writeValueAsString(input);
	}	
	
}
