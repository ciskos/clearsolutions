package io.gitlab.ciskos.clearsolutions.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.gitlab.ciskos.clearsolutions.model.Users;

@Repository
public interface UsersRepository extends CrudRepository<Users, Long> {
	
	List<Users> findByBirthdayBetween(LocalDate from, LocalDate to);

}
