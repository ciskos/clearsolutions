package io.gitlab.ciskos.clearsolutions.service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.clearsolutions.model.Users;
import io.gitlab.ciskos.clearsolutions.repository.UsersRepository;

@Service
public class UsersService {

	@Autowired
	private UsersRepository usersRepository;
	
	public List<Users> getAllUsers() {
		return StreamSupport.stream(usersRepository.findAll().spliterator(), false).toList();
	}

	public Users getUsersById(Long usersId) {
		return usersRepository.findById(usersId).orElse(new Users());
	}
	
	public List<Users> getUsersByDateRange(LocalDate from, LocalDate to) {
		return usersRepository.findByBirthdayBetween(from, to);
	}

	public Users addUsers(Users users) {
		return usersRepository.save(users);
	}

	public Users updateUsers(Users users) {
		return usersRepository.save(users);
	}

	public void deleteUsersById(Long usersId) {
		usersRepository.deleteById(usersId);
	}
	
}
