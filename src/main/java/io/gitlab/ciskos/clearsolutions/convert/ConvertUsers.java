package io.gitlab.ciskos.clearsolutions.convert;

import io.gitlab.ciskos.clearsolutions.dto.UsersDto;
import io.gitlab.ciskos.clearsolutions.model.Users;

public class ConvertUsers {

	public static UsersDto toDto(Users users) {
		return new UsersDto(users.getId()
							, users.getEmail()
							, users.getFirstName()
							, users.getLastName()
							, users.getBirthday()
							, users.getAddress()
							, users.getPhoneNumber());
	}

	public static Users toEntity(UsersDto usersDto) {
		return new Users(usersDto.getId()
						, usersDto.getEmail()
						, usersDto.getFirstName()
						, usersDto.getLastName()
						, usersDto.getBirthday()
						, usersDto.getAddress()
						, usersDto.getPhoneNumber());
	}
	
}
