package io.gitlab.ciskos.clearsolutions.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import io.gitlab.ciskos.clearsolutions.convert.ConvertUsers;
import io.gitlab.ciskos.clearsolutions.dto.SearchByDateRangeDto;
import io.gitlab.ciskos.clearsolutions.dto.UsersDto;
import io.gitlab.ciskos.clearsolutions.service.UsersService;
import io.gitlab.ciskos.clearsolutions.utility.JsonHandler;
import jakarta.validation.Valid;

@RestController
@RequestMapping(value = UsersRestController.USERS_PATH, produces = "application/json")
public class UsersRestController {
	
	static final String USERS_PATH = "/users";
	
	@Autowired
	private UsersService usersService;

	@GetMapping
	public ResponseEntity<String> getAllUserss() throws JsonProcessingException {
		var allUsers = usersService.getAllUsers();

		return ResponseEntity.ok(JsonHandler.objectToJson(allUsers.stream()
													.map(ConvertUsers::toDto)
													.toList()));
	}

	@GetMapping("/{id}")
	public ResponseEntity<String> getUsersById(@PathVariable("id") Long id) throws JsonProcessingException {
		var users = usersService.getUsersById(id);
		
		if (users.getId() != null) {
			return ResponseEntity.ok(JsonHandler.objectToJson(ConvertUsers.toDto(users)));
		}
		
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}

	@PostMapping(path = "/daterange", consumes = "application/json")
	public ResponseEntity<String> getAllusersByDateRange(
			@Valid @RequestBody SearchByDateRangeDto searchByDateRangeDto
			, BindingResult bindingResult) throws JsonProcessingException {
		if (bindingResult.hasErrors()) {
	        return new ResponseEntity<>(JsonHandler.bindingResultToJson(bindingResult), HttpStatus.BAD_REQUEST);
	    }
		
		var usersByDateRange = usersService.getUsersByDateRange(searchByDateRangeDto.getRange().getFrom()
									, searchByDateRangeDto.getRange().getTo());
		
		return ResponseEntity.ok(JsonHandler.objectToJson(usersByDateRange.stream()
													.map(ConvertUsers::toDto)
													.toList()));
	}

	@PostMapping(consumes = "application/json")
	public ResponseEntity<String> addUsers(
			@Valid @RequestBody UsersDto usersDto
			, BindingResult bindingResult) throws JsonProcessingException {
		if (bindingResult.hasErrors()) {
	        return new ResponseEntity<>(JsonHandler.bindingResultToJson(bindingResult), HttpStatus.BAD_REQUEST);
	    }
		
		var users = ConvertUsers.toEntity(usersDto);
		
		return ResponseEntity.ok(JsonHandler.objectToJson(ConvertUsers.toDto(usersService.addUsers(users))));
	}
	
	@PutMapping
	public ResponseEntity<String> putUsers(
			@Valid @RequestBody UsersDto usersDto
			, BindingResult bindingResult) throws JsonProcessingException {
		if (bindingResult.hasErrors()) {
	        return new ResponseEntity<>(JsonHandler.bindingResultToJson(bindingResult), HttpStatus.BAD_REQUEST);
	    }

		var users = ConvertUsers.toEntity(usersDto);
		
		return ResponseEntity.ok(JsonHandler.objectToJson(ConvertUsers.toDto(usersService.updateUsers(users))));
	}

	@PatchMapping(path = "/{usersId}", consumes = "application/json")
	public ResponseEntity<String> patchUsers(
			@PathVariable("usersId") Long usersId
			, @RequestBody UsersDto usersDto) throws JsonProcessingException {
		var users = usersService.getUsersById(usersId);
		
		if (usersDto.getEmail() != null) users.setEmail(usersDto.getEmail());
		if (usersDto.getFirstName() != null) users.setFirstName(usersDto.getFirstName());
		if (usersDto.getLastName() != null) users.setLastName(usersDto.getLastName());
		if (usersDto.getBirthday() != null) users.setBirthday(usersDto.getBirthday());
		if (usersDto.getAddress() != null) users.setAddress(usersDto.getAddress());
		if (usersDto.getPhoneNumber() != null) users.setPhoneNumber(usersDto.getPhoneNumber());

		return ResponseEntity.ok(JsonHandler.objectToJson(ConvertUsers.toDto(usersService.updateUsers(users))));
	}

	@DeleteMapping("/{usersId}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteUsers(@PathVariable("usersId") Long usersId) throws Exception {
		try {
			usersService.deleteUsersById(usersId);
		} catch (EmptyResultDataAccessException e) {
			throw new Exception();
		}
	}

}
