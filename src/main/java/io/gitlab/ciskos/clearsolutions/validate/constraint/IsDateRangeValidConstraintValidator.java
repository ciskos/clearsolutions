package io.gitlab.ciskos.clearsolutions.validate.constraint;

import io.gitlab.ciskos.clearsolutions.model.SearchByDateRange;
import io.gitlab.ciskos.clearsolutions.validate.IsDateRangeValid;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class IsDateRangeValidConstraintValidator implements ConstraintValidator<IsDateRangeValid, SearchByDateRange> {

	@Override
	public boolean isValid(SearchByDateRange range, ConstraintValidatorContext context) {
		return range.getFrom().isBefore(range.getTo());
	}
	
}