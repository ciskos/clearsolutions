package io.gitlab.ciskos.clearsolutions.validate.constraint;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Value;

import io.gitlab.ciskos.clearsolutions.validate.OlderThan18;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class OlderThan18ConstraintValidator implements ConstraintValidator<OlderThan18, LocalDate> {

	@Value("${age.from}")
	private Integer age;
	
	@Override
	public boolean isValid(LocalDate date, ConstraintValidatorContext context) {
		return date.isBefore(LocalDate.now().minusYears(age));
	}
	
}