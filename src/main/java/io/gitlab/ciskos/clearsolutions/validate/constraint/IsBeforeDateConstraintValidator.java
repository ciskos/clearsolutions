package io.gitlab.ciskos.clearsolutions.validate.constraint;

import java.time.LocalDate;

import io.gitlab.ciskos.clearsolutions.validate.IsBeforeDate;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class IsBeforeDateConstraintValidator implements ConstraintValidator<IsBeforeDate, LocalDate> {

	@Override
	public boolean isValid(LocalDate date, ConstraintValidatorContext context) {
		return date.isBefore(LocalDate.now());
	}
	
}