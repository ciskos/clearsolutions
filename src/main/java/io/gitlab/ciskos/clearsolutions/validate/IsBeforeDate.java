package io.gitlab.ciskos.clearsolutions.validate;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import io.gitlab.ciskos.clearsolutions.validate.constraint.IsBeforeDateConstraintValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

@Constraint(validatedBy = IsBeforeDateConstraintValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD } )
@Retention(RetentionPolicy.RUNTIME)
public @interface IsBeforeDate {

	public String message() default "Date must be less than current";
	
	public Class<?>[] groups() default {};
	
	public Class<? extends Payload>[] payload() default {};

}