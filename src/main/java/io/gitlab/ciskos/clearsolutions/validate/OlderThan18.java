package io.gitlab.ciskos.clearsolutions.validate;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import io.gitlab.ciskos.clearsolutions.validate.constraint.OlderThan18ConstraintValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

@Constraint(validatedBy = OlderThan18ConstraintValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD } )
@Retention(RetentionPolicy.RUNTIME)
public @interface OlderThan18 {

	public String message() default "User must be older than 18";
	
	public Class<?>[] groups() default {};
	
	public Class<? extends Payload>[] payload() default {};

}