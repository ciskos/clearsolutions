package io.gitlab.ciskos.clearsolutions.exception;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorMessage {

	private Integer statusCode;
	private LocalDate timestamp;
	private String message;
	private String description;

}