package io.gitlab.ciskos.clearsolutions.model;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchByDateRange {

	private LocalDate from;
	private LocalDate to;
	
}
