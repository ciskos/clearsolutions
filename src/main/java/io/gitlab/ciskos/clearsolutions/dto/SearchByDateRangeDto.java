package io.gitlab.ciskos.clearsolutions.dto;

import io.gitlab.ciskos.clearsolutions.model.SearchByDateRange;
import io.gitlab.ciskos.clearsolutions.validate.IsDateRangeValid;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchByDateRangeDto {

	@NotNull
	@IsDateRangeValid
	private SearchByDateRange range;
	
}
