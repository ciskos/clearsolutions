package io.gitlab.ciskos.clearsolutions.dto;

import java.time.LocalDate;

import io.gitlab.ciskos.clearsolutions.validate.IsBeforeDate;
import io.gitlab.ciskos.clearsolutions.validate.OlderThan18;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UsersDto {

	private Long id;

	@NotNull(message = "required")
	@NotBlank(message = "must not be blank")
	@Email(regexp = ".+@.+\\..+", message = "invalid email format")
	private String email;

	@NotNull(message = "required")
	@NotBlank(message = "must not be blank")
	private String firstName;
	
	@NotNull(message = "required")
	@NotBlank(message = "must not be blank")
	private String lastName;

	@IsBeforeDate
	@OlderThan18
	private LocalDate birthday;

	@NotBlank(message = "must not be blank")
	private String address;

	/*
	 * Valid Patterns:
	 * +3 8 063 123 1234
	 * +38 063 123 1234
	 * +3 8 063 123 12 34
	 * +3 8 (063) 123 12 34
	 * +38(063) 123 12 34
	 * +38(063)123 12 34
	 * 
	 * +3-8-063-123-1234
	 * +38-063-123-1234
	 * +3-8-063-123-12-34
	 * +3-8-(063)-123-12-34
	 * +38(063)-123-12-34
	 * +38(063)123-12-34
	 * */
	@NotBlank(message = "must not be blank")
	@Pattern(regexp = "\\+3[\\s-]?8[\\s-]?\\(?(\\d{3})\\)?[\\s-]?(\\d{3})[\\s-]?(\\d{2})[\\s-]?(\\d{2})[\\s-]?", message = "invalid phone format")
	private String phoneNumber;
	
}
