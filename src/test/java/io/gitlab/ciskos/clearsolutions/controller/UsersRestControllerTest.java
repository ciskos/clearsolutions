package io.gitlab.ciskos.clearsolutions.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class UsersRestControllerTest {


	private static final Long TEST_USERS_ID = 1L;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	@DisplayName("Test get all users")
	void testGetAllUserss() throws Exception {
		var result = "[{\"id\":1,\"email\":\"email1@mail.com\",\"firstName\":\"firstname1\",\"lastName\":\"lastname1\",\"birthday\":[1980,1,1],\"address\":\"address1\",\"phoneNumber\":\"+38(063)123-12-34\"},{\"id\":2,\"email\":\"email2@mail.com\",\"firstName\":\"firstname2\",\"lastName\":\"lastname2\",\"birthday\":[1981,2,2],\"address\":\"address2\",\"phoneNumber\":\"+38(063)123 12 34\"},{\"id\":3,\"email\":\"email3@mail.com\",\"firstName\":\"firstname3\",\"lastName\":\"lastname3\",\"birthday\":[1982,3,3],\"address\":\"address3\",\"phoneNumber\":\"+38(063) 123 12 34\"},{\"id\":4,\"email\":\"email4@mail.com\",\"firstName\":\"firstname4\",\"lastName\":\"lastname4\",\"birthday\":[1983,4,4],\"address\":\"address4\",\"phoneNumber\":\"+38(063)-123-12-34\"}]";
		
		mockMvc.perform(get("/users")
				.accept(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().string(containsString(result)));
	}

	@Test
	@DisplayName("Test get users by id")
	void testGetUsersById() throws Exception {
		var result = "{\"id\":1,\"email\":\"email1@mail.com\",\"firstName\":\"firstname1\",\"lastName\":\"lastname1\",\"birthday\":[1980,1,1],\"address\":\"address1\",\"phoneNumber\":\"+38(063)123-12-34\"}";
		
		mockMvc.perform(get("/users/{id}", TEST_USERS_ID)
				.accept(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().string(containsString(result)));
	}

	@Test
	@DisplayName("Test get all users by date range")
	void testGetAllusersByDateRange() throws Exception {
		var content = "{\"range\":{\"from\":\"1900-01-01\",\"to\":\"2000-01-01\"}}";
		var result = "[{\"id\":1,\"email\":\"email1@mail.com\",\"firstName\":\"firstname1\",\"lastName\":\"lastname1\",\"birthday\":[1980,1,1],\"address\":\"address1\",\"phoneNumber\":\"+38(063)123-12-34\"},{\"id\":2,\"email\":\"email2@mail.com\",\"firstName\":\"firstname2\",\"lastName\":\"lastname2\",\"birthday\":[1981,2,2],\"address\":\"address2\",\"phoneNumber\":\"+38(063)123 12 34\"},{\"id\":3,\"email\":\"email3@mail.com\",\"firstName\":\"firstname3\",\"lastName\":\"lastname3\",\"birthday\":[1982,3,3],\"address\":\"address3\",\"phoneNumber\":\"+38(063) 123 12 34\"},{\"id\":4,\"email\":\"email4@mail.com\",\"firstName\":\"firstname4\",\"lastName\":\"lastname4\",\"birthday\":[1983,4,4],\"address\":\"address4\",\"phoneNumber\":\"+38(063)-123-12-34\"}]";
		
		mockMvc.perform(post("/users/daterange")
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().string(containsString(result)));
	}

	@Test
	@DirtiesContext
	@DisplayName("Test add users")
	void testAddUsers() throws Exception {
		var content = "{\"email\":\"emailNEW@mail.com\",\"firstName\":\"firstname5\",\"lastName\":\"lastname5\",\"birthday\":[1980,5,5],\"address\":\"address5\",\"phoneNumber\":\"+38(098)321-21-43\"}";
		var result = "{\"id\":5,\"email\":\"emailNEW@mail.com\",\"firstName\":\"firstname5\",\"lastName\":\"lastname5\",\"birthday\":[1980,5,5],\"address\":\"address5\",\"phoneNumber\":\"+38(098)321-21-43\"}";
		
		mockMvc.perform(post("/users")
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().string(containsString(result)));
	}

	@Test
	@DirtiesContext
	@DisplayName("Test put users")
	void testPutUsers() throws Exception {
		var content = "{\"id\":4,\"email\":\"emailNEW@mail.com\",\"firstName\":\"firstnameNEW\",\"lastName\":\"lastnameNEW\",\"birthday\":[2000,5,5],\"address\":\"addressNEW\",\"phoneNumber\":\"+38(098)1234567\"}";
		var result = "{\"id\":4,\"email\":\"emailNEW@mail.com\",\"firstName\":\"firstnameNEW\",\"lastName\":\"lastnameNEW\",\"birthday\":[2000,5,5],\"address\":\"addressNEW\",\"phoneNumber\":\"+38(098)1234567\"}";

		mockMvc.perform(put("/users")
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().string(containsString(result)));
	}

	@Test
	@DirtiesContext
	@DisplayName("Test patch users")
	void testPatchUsers() throws Exception {
		var content = "{\"email\":\"emailPATCHED@mail.com\",\"birthday\":[2000,6,6],\"phoneNumber\":\"+38(063)7654321\"}";
		var result = "{\"id\":1,\"email\":\"emailPATCHED@mail.com\",\"firstName\":\"firstname1\",\"lastName\":\"lastname1\",\"birthday\":[2000,6,6],\"address\":\"address1\",\"phoneNumber\":\"+38(063)7654321\"}";

		mockMvc.perform(patch("/users/{id}", TEST_USERS_ID)
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().string(containsString(result)));
	}

	@Test
	@DirtiesContext
	@DisplayName("Test delete users")
	void testDeleteUsers() throws Exception {
		mockMvc.perform(delete("/users/{id}", TEST_USERS_ID)
				.accept(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().is2xxSuccessful());
	}

	@Test
	@DisplayName("Test for exceptions")
	void testExceptiond() throws Exception {
		var content = "{...}";
		var result = "{\"statusCode\":400,\"timestamp\":[2023,9,28],\"message\":\"JSON parse error: Unexpected character ('.' (code 46)): was expecting double-quote to start field name\",\"description\":\"uri=/users/1\"}";

		mockMvc.perform(patch("/users/{id}", TEST_USERS_ID)
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
				.andDo(print())
				.andExpect(status().is4xxClientError())
				.andExpect(content().string(containsString(result)));
	}
	
}
